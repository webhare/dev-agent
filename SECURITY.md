# Security considerations

## WebHare authentication

We must prevent random sites from registering themselves with connect helper.

To achieve this:
- registration goes through `/siteapi`
- `/siteapi` does not accept CORS requests
- `/siteapi` requires requests to be in JSON format, so form posts cannot succeed
- a user must authorize a site on the localhost origin, before it's allowed to communicate with the dev-agent
- when a server is authorized, it's informed of the callback URL and given an authentication token using the `__webhare_connect__` variable
- `/connectapi`, which does accept CORS requests, requires the token as a `Authentication: Bearer` header. It will not process
  requests if the `Origin` header is missing or if the Origin is currently registered with a different token
