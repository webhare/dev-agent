#!/bin/sh

if [[ "$1" =~ ^/Volumes/[A-Za-z0-9.-]+$ ]]; then
  mkdir "$1"
  chown "$2" "$1"
  exit 0
fi

exit 1
