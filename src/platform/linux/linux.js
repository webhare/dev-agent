"use strict";

//let Fs = require('fs');
let Process = require('process');
let path = require('path');
let childprocess = require('child_process');
let support = require('../support');


module.exports.doReveal = function(url, login, password, folder)
{
  console.log('doReveal', url, login, password, folder);

/*
  let volumepath = "/Volumes/" + url.split('//')[1].split('/')[0].replace(':','-');
  let mustmount = false;

  try
  {
    if(Fs.readdirSync(volumepath).length == 0)
      mustmount=true;
  }
  catch(e)
  {
    Fs.mkdirSync(volumepath);
    mustmount=true;
  }

  if(mustmount)
  {
    //note: the '-a0' api has been deprecated since OSX 10.4 so it might go away at some point
    //http://www.opensource.apple.com/source/webdavfs/webdavfs-368/mount.tproj/webdav_agent.c is the implementation, discovered proxy username/pass there
    let tmpobj = require('tmp').fileSync();
    let loginlen = Buffer.byteLength(login);
    let passwordlen = Buffer.byteLength(password);
    let credentials = new Buffer(16 + loginlen + passwordlen);

    let offset = 0;
    credentials.writeInt32BE(loginlen, offset); offset += 4;
    offset += credentials.write(login, offset);
    credentials.writeInt32BE(passwordlen, offset); offset += 4;
    offset += credentials.write(password, offset);
    //proxy user
    credentials.writeInt32BE(0, offset); offset += 4;
    //proxy password
    credentials.writeInt32BE(0, offset); offset += 4;

    Fs.writeSync(tmpobj.fd, credentials, 0, credentials.length);

    new Promise(function (resolve,reject)
    {
      let mounter = require('child_process').spawn("mount_webdav", ["-a","3", url, volumepath ], { stdio: [null, null, process.stderr, tmpobj.fd ]});
      mounter.on('exit', resolve);
//      mounter.stdin.write(credentials);
    }).then(function(result)
    {
      console.error(result);
    });
  }

  let destpath = volumepath + folder;
  require('child_process').spawn("osascript", ['-e','set thePath to POSIX file "' + destpath + '"','-e','tell application "Finder" to reveal thePath','-e','tell application "Finder" to activate']);
*/
};


module.exports.doOpenAsset = async function(cmd)
{
  //cmd == {"type":"reveal","item":"/","login":"sysop","password":"***","url":"https://webhare.moe.sf.b-lex.com/webdav/","data":null,"WebHareConnect":{"version":1}}
  console.log(cmd);

  let islocal = cmd.localdata && support.isLocalByIps(cmd.localdata.ips);
  let localpath = "";

  if (islocal)
  {
    let env = {};
    if (cmd.localdata.config)
      env.WEBHARE_CONFIG = cmd.localdata.config;
    if (cmd.localdata.dataroot)
      env.WEBHARE_DATAROOT = cmd.localdata.dataroot;
    localpath = await support.lookupLocalResourcePath(cmd.item, cmd.localdata.wh, env);
  }
  if (!localpath)
  {
    // mount data

  }
  if (!localpath)
  {
    console.log(`Could not determine local path`);
    return;
  }

  if (cmd.type === "editor")
    openInEditor(localpath, cmd.data);
  else if (cmd.type == "reveal")
    openInDolphin(localpath);
};

function openInEditor(item, data)
{
  if(data.line)
  {
    item+=':'+data.line;
    if(data.col)
      item+=':'+data.col;
  }

  childprocess.spawn("/opt/src/sublime_text_3/sublime_text"
                                , [ item ]
                                , { detached: true } );
}

function openInDolphin(item)
{
  childprocess.spawn("dolphin"
                                , [ item ]
                                , { detached: true } );
}


module.exports.install = function()
{
  throw new Error(`Install ${Process.argv[0]} ${Process.argv[1]}`);
};

module.exports.uninstall = function()
{
  throw new Error(`Uninstall ${Process.argv[0]} ${Process.argv[1]}`);
};

