#!/usr/bin/env node
"use strict";

let Fs = require('fs');
let Process = require('process');
const path = require('path');
let ChildProcess = require('child_process');
let support = require('../support');
const notifier = require('node-notifier');

// init
var plistpath = path.join(Process.env['HOME'], "Library/LaunchAgents");
var plistfile = path.join(plistpath,'dev.webhare.dev-agent.plist');

function getCreateMountpointPath()
{
  return path.resolve(__dirname,"../../create-mountpoint.sh");
}

function getVolumePathFromURL(url)
{
  return "/Volumes/" + url.split('//')[1].split('/')[0].replace(':','-');
}

async function ensureMount(volumepath, login, password, url, options)
{
  let mustmount = false;
  try
  {
    if(Fs.readdirSync(volumepath).length == 0)
      mustmount=true;
  }
  catch(e)
  {
    mustmount=true;
  }

  if(mustmount)
  {
    let username = process.env["USER"] || ("" + ChildProcess.execSync("whoami")).trim();
    let launchargs = [ getCreateMountpointPath(), volumepath, username ];
    if(options && options.debug)
      console.log("Launching sudo " + launchargs.join(" "));
    ChildProcess.spawnSync("sudo", launchargs, { stdio:['ignore','inherit','inherit']});

    /*
    we used to use -a0, but that broke in OSX Sierra 10.12.5 (it has been deprecated since OSX 10.4). see de41f748366a49961a72fb5f13a97cff5ff978b2 if you want it back
    switch to an expect solution based on this one: https://github.com/childrss/webdav/blob/master/keep-webdav-munki-repo-mounted.exp
    */
    let result = await new Promise(function (resolve,reject)
    {
      let args = ["-a","3", url, volumepath ];

      let mounter = ChildProcess.spawn(__dirname + "/mount_webdav.exp", [ url, volumepath, login, password ], { stdio:['ignore','inherit','inherit'] });
      mounter.on('exit', resolve);
    });

    notifier.notify(result ? "Mount failed, error code: " + result : 'Mounted!');
  }
  else
  {
    notifier.notify('Already mounted..');

  }
}

function revealInFinder(destpath)
{
  console.log("revealInFinder",destpath);

  //ADDME if it's a filepath, tell finder to select that file!
  destpath = destpath.substr(0, destpath.lastIndexOf("/")+1);

  console.log("resolved to",destpath);

  ChildProcess.spawn("osascript", ['-e','set thePath to POSIX file "' + destpath + '"'
                                  ,'-e','tell application "Finder" to reveal thePath'
                                  ,'-e','tell application "Finder" to activate'
                                  ]);
}

function openInEditor(item, data)
{
  if(data.line)
  {
    item+=':'+data.line;
    if(data.col)
      item+=':'+data.col;
  }

  ChildProcess.spawn("/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl"
                                , [ item ]
                                , { detached: true } );
}

module.exports.doOpenAsset = async function(cmd, options)
{
  //cmd == {"type":"reveal","item":"/","login":"sysop","password":"***","url":"https://webhare.moe.sf.b-lex.com/webdav/","data":null,"WebHareConnect":{"version":1}}
  let islocal = cmd.localdata && support.isLocalByIps(cmd.localdata.ips);
  let localpath = "";

  if (islocal)
  {
    let env = {};
    if (cmd.localdata.config)
      env.WEBHARE_CONFIG = cmd.localdata.config;
    if (cmd.localdata.dataroot)
      env.WEBHARE_DATAROOT = cmd.localdata.dataroot;
    localpath = await support.lookupLocalResourcePath(cmd.item, cmd.localdata.wh, env);
  }
  if (!localpath)
  {
    // mount data
    let volumepath = getVolumePathFromURL(cmd.url);
    await ensureMount(volumepath, cmd.login, cmd.password, cmd.url, options);
    let item = cmd.item;
    localpath = volumepath + item;
  }
  if (!localpath)
    return;

  if(cmd.type == 'reveal')
    revealInFinder(localpath);
  else if(cmd.type == 'editor')
    openInEditor(localpath, cmd.data);

  //First get the requested webdav mount in place
};

module.exports.install = function(args)
{
  //we cant use Process.argv[0], brew points it to a versioned dir which may go away, not /usr/local/bin/node
  const helperpath = path.resolve(__dirname,"start-dev-agent.sh");

  var plistcontents =
  `<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>Label</key>
  <string>dev.webhare.dev-agent</string>
  <key>Disabled</key>
  <false/>
  <key>ProgramArguments</key>
  <array>
    <string>${helperpath}</string>
  </array>
  <key>KeepAlive</key>
  <true/>
</dict>
</plist>`;

  if(args.debug)
  {
    console.log("plist to install:");
    console.log(plistcontents);
  }

  if(!Fs.existsSync(plistpath))
    Fs.mkdirSync(plistpath);

  if(args.debug)
    console.log(`Writing plist to '${plistfile}'`);
  Fs.writeFileSync(plistfile, plistcontents);

  if(args.debug)
    console.log(`Unloading service`);
  ChildProcess.spawnSync("launchctl", ["unload", plistfile], { stdio:['ignore','ignore','ignore']});

  if(args.debug)
    console.log(`Loading service`);
  ChildProcess.spawnSync("launchctl", ["load", plistfile], { stdio:['ignore','inherit','inherit']});
};

module.exports.uninstall = function()
{
  ChildProcess.spawnSync("launchctl", ["unload", plistfile], { stdio:'inherit'});
  try
  {
    Fs.unlinkSync(plistfile);
  }
  catch(e)
  {

  }
};

module.exports.installSudo = function()
{
  let contents = `# sudoers file installed by webhare-dev-agent
%admin ALL=(ALL) NOPASSWD: ${getCreateMountpointPath()}
`;

  Fs.writeFileSync("/etc/sudoers.d/webhare-dev-agent", contents);
}
