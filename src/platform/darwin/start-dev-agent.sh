#!/bin/bash
export SRCFOLDER="`cd ${BASH_SOURCE%/*}/../..;pwd`"
echo $SRCFOLDER

if [ -x /usr/local/bin/node ]; then
  exec /usr/local/bin/node $SRCFOLDER/dev-agent.js "$@"
elif [ -x /usr/bin/node ]; then
  exec /usr/local/bin/node $SRCFOLDER/dev-agent.js "$@"
else
  echo "Cannot find node binary"
  exit 1
fi
