const child_process = require('child_process');
const os = require('os');
const path = require('path');


module.exports.isLocalByIps = function(ips)
{
  var ifaces = os.networkInterfaces();

  let localips = [];
  for (let ifacename of Object.keys(ifaces))
  {
    for (let iface of ifaces[ifacename])
    {
      if (![ "IPv4", "IPv6", 4, 6 ].includes(iface.family) || iface.internal)
        continue;

      localips.push(iface.address);
    }
  }

  for (let addr of ips)
    if (localips.includes(addr))
      return true;

  return false;
};

module.exports.lookupLocalResourcePath = async function(item, whbin, env)
{
  const modulewebdavprefix = "/system/modules/";
  let modulepath = "";
  if (item.startsWith(modulewebdavprefix))
    modulepath = item.substr(modulewebdavprefix.length);
  else
  {
    const match = /^(mod[^:/]*)::([^/]*)(\/.*)$/.exec(item);
    if (match)
    {
      if (match[1] == "mod")
        modulepath = `${match[2]}${match[3]}`;
      else if (match[1] == "modulescript")
        modulepath = `${match[2]}/scripts${match[3]}`;
      else if (match[1] == "moduledata")
        modulepath = `${match[2]}/data${match[3]}`;
      else if (match[1] == "module")
        modulepath = `${match[2]}/include${match[3]}`;
    }
  }

  if (!modulepath)
    return "";

  return await new Promise((resolve, reject) =>
    {
      let whenv = Object.assign({}, process.env, env);
      let result = "";
      let lookup = child_process.spawn(whbin, [ "getmoduledir", modulepath ], { cwd: path.dirname(whbin), env: whenv });
      lookup.stdout.on('data', (data) => result += data);
      lookup.on('exit', () => resolve(result.trim()));
    });
};

