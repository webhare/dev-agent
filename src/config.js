const crypto = require('crypto');
const fs = require('fs');
const util = require('util');
const path = require('path');
const configfilepath = path.join(require('os').homedir(), '.webhareconnect');

let configuration = null;

function getOrigin(url)
{
  return url.split('/').slice(0,3).join('/');
}

async function readConfiguration()
{
  try
  {
    let configtext = await util.promisify(fs.readFile)(configfilepath);
    configuration = JSON.parse(configtext);
  }
  catch(e)
  {
    configuration = {};
  }

  configuration = { servers: []
                  , ...configuration
                  };
}
async function writeConfiguration()
{
  await util.promisify(fs.writeFile)(configfilepath+'.tmp', JSON.stringify(configuration));
  await util.promisify(fs.rename)(configfilepath+'.tmp', configfilepath);
}

async function getSecretSeed()
{
  if(!configuration)
    await readConfiguration();
  if(!configuration.secretseed)
  {
    configuration.secretseed = (await util.promisify(crypto.randomBytes)(16)).toString('base64');
    await writeConfiguration();
  }
  return configuration.secretseed;
}

async function calcServerHash(origin, nonce)
{
  await readConfiguration();
  let mysecretseed = await getSecretSeed();
  let hash = crypto.createHash('sha256');

  hash.update(mysecretseed);
  hash.update(origin);
  hash.update(nonce);

  return hash.digest('base64');
}

async function getTokenForServer(serverurl)
{
  let origin = getOrigin(serverurl);
  let nonce = (await util.promisify(crypto.randomBytes)(16)).toString('base64');
  let hash = await calcServerHash(origin,nonce);
  return `WHC-ST:${nonce}:${hash}`;
}

async function isValidToken(origin, token)
{
  let tokparts = token.split(':');
  return tokparts.length == 3 && tokparts[0] == 'WHC-ST' && tokparts[2] == await calcServerHash(origin, tokparts[1]);
}

async function registerServer(serverurl, token)
{
  let origin = getOrigin(serverurl);
  if(!await isValidToken(origin,token))
    throw new Error("Invalid token");

  let serverentry = configuration.servers.find(entry => entry.origin == origin);
  if(serverentry)
    serverentry.token = token;
  else
    configuration.servers.push({origin,token});

  await writeConfiguration();
}

async function verifyServer(serverurl, token)
{
  let origin = getOrigin(serverurl);
  if(!await isValidToken(origin,token))
    return false;

  let serverentry = configuration.servers.find(entry => entry.origin == origin);
  return serverentry && serverentry.token == token;
}

async function getRegisteredServers()
{
  await readConfiguration();
  return configuration.servers;
}

module.exports = { getTokenForServer
                 , registerServer
                 , verifyServer
                 , getRegisteredServers
                 };
