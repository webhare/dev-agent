#!/usr/bin/env node
"use strict";

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('./config');

const platformsupport = require(`./platform/${process.platform}/${process.platform}.js`);
let callbackurl;
let debug;

async function executeConnectRequest(request)
{
  if(request.method == 'openAsset')
  {
    return await platformsupport.doOpenAsset(request, {debug:debug});
  }
  if(request.method == 'getRegisteredServers')
  {
    //filter it, shouldn't leak tokens etc
    return (await config.getRegisteredServers()).map (node => ({ url: node.origin }));
  }
  if(request.method == 'register')
  {
    return null;
  }
  throw new Error(`Unknown connect method '${request.method}'`);
}

async function executeSiteRequest(request)
{
  if(request.method == 'getToken')
  {
    let token = await config.getTokenForServer(request.url);
    return { token };
  }
  if(request.method == 'registerServer')
  {
    await config.registerServer(request.url, request.token);
    return { success: true };
  }
  throw new Error(`Unknown siteapi method '${request.method}'`);
}

async function exceuteAnyAPIRequest(req, res, handler)
{
  try
  {
    if(debug)
      console.log("Receiving request", req.body);

    if(!req.get('content-type') == 'application/json')
      throw new Error("application/json is required");

    let response = await handler(req.body);
    if(debug)
      console.log("Response", response);

    res.set('content-type','application/json');
    res.send(JSON.stringify({status:"ok",...response}));
    return;
  }
  catch(e)
  {
    if(debug)
      console.log("Exception",e);

    res.status(500).send({"status":"error","error":e.toString()}).end();
  }
}

async function onConnectAPIRequest(req, res)
{
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  if(req.method=='OPTIONS')
  {
    res.end();
    return;
  }

  let origin = req.get("Origin");
  if(!origin)
  {
    res.status(400).send({error:"An 'Origin' heeader is required"});
    return;
  }

  let auth = req.get('Authorization');
  if(!auth || !auth.toUpperCase().startsWith("BEARER ") || !await config.verifyServer(origin, auth.substr(7)))
  {
    res.status(401).send({error:"Invalid Authorization"});
    return;
  }

  return exceuteAnyAPIRequest(req,res, executeConnectRequest);
}

async function onSiteAPIRequest(req, res)
{
  //we rely on XHR protections and won't specify CORS headers
  return exceuteAnyAPIRequest(req,res, executeSiteRequest);
}

let opt = require('node-getopt').create([
  ['p' , 'port=PORT'           , 'port, defaults to 7521'],
  ['s' , 'servername=HOST'     , 'server name, defaults to 127.0.0.1'],
  ['h' , 'help'                , 'display this help'],
  ['d' , 'debug'               , 'debug mode'],
  [''  , 'install'             , 'install as service'],
  [''  , 'install-sudo'        , 'install necessary sudo privileges'],
  [''  , 'uninstall'           , 'uninstall service']
])              // create Getopt instance
.bindHelp()     // bind option 'help' to default action
.parseSystem(); // parse command line

let portnumber = parseInt(opt.options.port || '7521');
let servername = opt.options.servername || '127.0.0.1';
callbackurl = `http://${servername}:${portnumber}`;
debug = opt.options.debug;

if(opt.options.install)
  return void platformsupport.install({debug: debug});
if(opt.options["install-sudo"])
  return void platformsupport.installSudo({debug: debug});
if(opt.options.uninstall)
  return void platformsupport.uninstall({debug: debug});

app.all('/connectapi', bodyParser.json(), (req,res) => onConnectAPIRequest(req,res));
app.all('/siteapi', bodyParser.json(), (req,res) => onSiteAPIRequest(req,res));
app.use('/', express.static(__dirname + '/../site/'));
//app.get('/', (req, res) => res.send('Hello World!'))
app.listen(portnumber, servername, () =>
  {
    console.log(`Now running on ${callbackurl}`);
  });
