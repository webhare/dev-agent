import './connectsite.css';
import * as dompack from 'dompack';
import 'whatwg-fetch';


function resolve(srcurl, desturl)
{
  return (new URL(desturl, srcurl)).toString();
}

async function getConnectDetailsForServer(url)
{
  let connectdetails = await fetch('/siteapi', { method: 'POST'
                                               , body: JSON.stringify({ method: 'getToken', url: url })
                                               , headers: new Headers( { 'Content-Type': 'application/json' })
                                               });
  return connectdetails.json();
}

async function activateServer(url, token)
{
  await fetch('/siteapi', { method: 'POST'
                          , body: JSON.stringify({ method: 'registerServer', url: url, token: token })
                          , headers: new Headers( { 'Content-Type': 'application/json' })
                          });

  let connectsettings = { callback: resolve(location.href,'/'), token: token };
  location.href = url + "?__webhare_connect__=" + encodeURIComponent(JSON.stringify(connectsettings));
}

async function doConnectToServer(form, evt)
{
  dompack.stop(evt);
  if(!form.checkValidity())
    return;

  let url = resolve(form.elements.url.value,'/');
  let connectdetails = await getConnectDetailsForServer(url);
  activateServer(url, connectdetails.token);
  window.open('/connect/?url=' + encodeURIComponent(form.elements.url.value),'_blank');
}
dompack.register('#connectserver', node=>
{
  node.addEventListener("submit", evt => doConnectToServer(node, evt))
});

async function launchVerifyServer(serverurl)
{
  serverurl = resolve(serverurl,'/');
  dompack.qS('#verifyserver').classList.add('apppart--active');
  dompack.qSA('.verifyserver--serverurl').forEach(node => node.textContent = serverurl);

  //don't show perimssion yet..
  dompack.qS('#verifyserver-permissionpart').style.dispay = 'none';
  let connectdetails = await getConnectDetailsForServer(serverurl);

  dompack.qS('#verifyservername_ack').addEventListener("click", async function()
  {
    activateServer(serverurl, connectdetails.token);
  });

  dompack.qS('#verifyservername_nack').addEventListener("click",function()
  {
    history.back();
  });
}

function launchHomepage()
{
  dompack.qS('#connectserver').classList.add('apppart--active');
  let connectjslink = `javascript:(function(){location.href='${location.origin}/?connect=' + encodeURIComponent(location.origin);})()`;

  dompack.qSA('.connectserver--connecthyperlink').forEach( link => link.href = connectjslink);
  dompack.qSA('.connectserver--connectlinktext').forEach( link => link.textContent = connectjslink);
}

dompack.onDomReady(function()
{
  let url = new URL(location.href);
  if(url.searchParams.get("connect"))
  {
    launchVerifyServer(url.searchParams.get("connect"));
    return;
  }
  launchHomepage();

});
