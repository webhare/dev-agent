# dev-agent
The WebHare dev agent is a local 'helper' that allows your local WebHare to open files directly in your editor and to mount
remote WebDav servers. It is available on [gitlab](https://gitlab.com/webhare/dev-agent) and [npm](https://www.npmjs.com/package/@webhare/dev-agent)

## Installation
```bash
npm i -g @webhare/dev-agent
dev-agent --install

# If you want to support automatically mounting WebDav folder on macOS (OS X) - see below
sudo dev-agent --install-sudo
```

### About automatically mounting WebDav folder on macOS (OSX)
Starting with OS X Sierra, `/Volumes` is owned and only writeable by `root`. The dev agent uses a helper to create
mount points. This script is run using sudo, so to avoid having to type in your password when connecting, add a line to
`/etc/sudoers` containing something like:

```
%admin ALL=(ALL) NOPASSWD: /path/to/webhare-dev-agent/src/create-mountpoint.sh
```

with the correct path to the create-mountpoint.sh script. This will allow sudo to run the script as root without asking
for a password for users within the 'admin' group.

This is what `--install-sudo` does for you.

## Uninstallation
```bash
dev-agent --uninstall
sudo rm /etc/sudoers.d/webhare-dev-agent   # IF you used --install-sudo
npm u -g webhare-dev-agent
```

# Development

## Setting up dompack builder

This ensures the site is automatically rebuilt when you change the .es files

```
npm i
node_modules/.bin/dompack-builder -rw .
```

## Running the process
```
node src/dev-agent.js --debug
```

## Getting started/testing
1. Invoke `webhare-dev-agent` or run `node src/dev-agent.js`

2. Go to http://127.0.0.1:7521/
2. Setup the following bookmarklet: `javascript:(function(){location.href%3D%27http://127.0.0.1:7521/%3Fconnect%3D%27%2BencodeURIComponent(location.origin);})()`

3. Go to Webhare and
Click activate, click connect to local WebHareConnect  and enter https://connect-local.webhare.com:7521/ as URL

4. Refresh or go to a WebHare installation

5. Rightclick on the WebHare dashboard tab, and select 'Mount this server over WebDav'

If everything works fine, you'll see a Finder window pop up.
